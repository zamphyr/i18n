{
    "about": {
        "title": "About Zamphyr",
        "subtitle": "We’re at the genesis of modern education.<br> Questioning everything, bringing future makers to the world.<br> Our mission is to provide next generation tech education.",
        "menu": {
            "about": "About",
            "jobs": "Jobs",
            "team": "Team",
            "press": "Press"
        },
        "main": {
            "title": "Our vision",
            "subtitle": "Zamphyr is a platform where technical knowledge is available to everyone. Imagine if school met Wikipedia. Everyone can contribute and everyone can learn. We open source and crowdsource material to make a universally available school. We're the confluence of ideas.<br><br> Our strongest belief is that the education of tomorrow breaks the cult of the average. We will build around that belief to provide the first ever platform that truly is ⸗ School 2.0.",
            "insights": "Insights, ideas, and stories"
        },
        "press": {
            "title": "Media coverage"
        },
        "team": {
            "title": "Our team"
        },
        "jobs": {
            "title": "Open positions",
            "internship": "Internship at Zamphyr",
            "interninfo": "Zamphyr Internship Program is an opportunity to be a part of a revolutionary startup set out to change technology education as we know it. You will be working on open-source technologies in education, improving our platform and spreading the vision of universal educational change to people and developer communities.",
            "apply": "Apply now"
        },
        "contact": {
            "nav": {
                "title": "More + Zamphyr",
                "faq": "FAQ",
                "free": "Free tools",
                "gtc": "Help with translations",
                "contact": "Contact"
            },
            "title": "Location",
            "subtitle": "We're a remote-first company with a diverse team and global mindset. Our HQ is set up at the center of Ülemiste City Tehnopolis - the heart and soul of European & Estonian startup future. We're looking to change education worldwide and our global presence are our people."
        },
        "faq": {
            "title": "Frequently asked questions",
            "q1": "How is Zamphyr free?",
            "a1": "Zamphyr is made as a School 2.0 to provide a place for everyone to learn to code, learn to design and get technical skills for free. It's free because it's designed to be a crowdsourced school which is only goverened by Zamphyr. That means that we allow everyone to contribute and we grow from these contributions. We work with companies to provide enterprise solutions and their financial contributions allow us to fund the infrastructure, hosting and our offline activities. In essence the school is sustainable when all parties prosper from it, the students, the industry and Zamphyr.",
            "q2": "Who can contribute to Zamphyr?",
            "a2": "Zamphyr is open to the whole humanity and everyone can contribute. Individuals, companies, organizations, educational institutions and any other entity with the interest to aid development of free and  open source education. To make sure only valid and checked information is in the lessons, Zamphyr is structured as a meritocracy, and merit is established on contribution. We planted the first generation of contributors and they will be the first to accept new entries to the material. All contributions are public and subject to open discussion, and they are merged upon checking and acceptance by the community."
        }
    },
    "contribute": {
        "title": "Contribute to the School 2.0",
        "subtitle": "Share your knowledge and revolutionize technical education.<br> Influence the industry and shape engineers of tomorrow.<br> Help build an open source global school.",
        "start": "Let's start",
        "menu": {
            "individual": "As an individual",
            "organization": "As an organization"
        },
        "new": {
            "title": "Suggest new topic (BETA)",
            "subtitle": "You can suggest a new topic to the community. All your contributions will be public and open for discussion at Gitlab.com.",
            "name": "Name",
            "slug": "Slug",
            "language": "Language",
            "schema": "Schema",
            "start": "Let's start"
        },
        "panel": {
            "title": "Contributor panel",
            "updates": "Your updates",
            "topics": "Topics you have contributed to",
            "issues": "Git issues",
            "impacted": "You have impacted",
            "users": "users"
        },
        "main": {
            "filters": "Filters",
            "suggested": "Suggested for you",
            "suggestions": "Your suggestions",
            "title": "Build the learning material",
            "subtitle": "Share your knowledge and revolutionize technical education. Influence the industry and shape engineers of tomorrow.",
            "new": "Start a new topic",
            "contribute": "Contribute"
        },
        "cla": {
            "title": "Contribution license",
            "subtitle": "Thank you for contributing to Zamphyr! Like many open source projects, we need a contributor license agreement from you before we can merge in your changes. You only need to fill out this agreement once. You are not giving up your copyright in your work. You will need a Gitlab account to contribute.",
            "cause": "Join our cause",
            "accept": "Accept the CLA",
            "repo": "Repository",
            "lic1": "I irrevocably agree to release your contribution under the CC BY-SA 3.0 License and the GFDL. You agree that a hyperlink or URL is sufficient attribution under the Creative Commons license.",
            "agree": "I agree to the terms of this agreement",
            "verify": "Verify with Gitlab"
        }
    },
    "home": {
        "title": "School reimagined",
        "topics": "Topics",
        "path": "Find the best learning path",
        "pathfinder": "Try the Pathfinder",
        "paths": "Paths",
        "blog": "Blog posts",
        "loop": "Be in the loop",
        "newsletter": "We work hard alongside good people to change education as it stands &mdash; still, weak and shivering. We're the ones not afraid the future, we're the one shaping it. We want you to be a part of it, we're offering you an opportunity to follow our newsletter.",
        "email": "E-mail",
        "follow": "Follow our newsletter"
    },
    "panel": {
        "menu": {
            "users": "Users",
            "students": "Students",
            "mentors": "Mentors",
            "entities": "Entities",
            "learning": "Learning material",
            "courses": "Courses",
            "specializations": "Specializations",
            "categories": "Categories",
            "seed": "Seed import",
            "marketing": "Marketing",
            "campains": "Campains",
            "templates": "Templates",
            "coupons": "Coupons",
            "mail": "Mail templates",
            "send": "Send emails",
            "meta": "Meta",
            "press": "Press",
            "team": "Team",
            "links": "Links",
            "testimonials": "Testimonials",
            "oauth": "OAuth clients",
            "asha": "Asha",
            "questions": "Questions",
            "notifications": "Notifcations",
            "profile": "Profile",
            "logout": "Log out",
            "abtest": "AB tests"
        },
        "2fa": {
            "verify": "Verify",
            "key": "Key",
            "generate": "Generate",
            "backup": "Backup codes",
            "new": "Generate new"
        },
        "app": {
            "modify": "Modify",
            "basic": "Basic info",
            "id": "Client ID",
            "secret": "Client Secret",
            "active": "Active",
            "redirect": "Redirect URI",
            "desc": "Description",
            "image": "Image",
            "owner": "Owner",
            "change": "Change",
            "api": "Access to private API"
        },
        "apps": {
            "title": "OAuth Apps",
            "name": "Name",
            "desc": "Description"
        },
        "email": {
            "title": "Send an email",
            "to": "To",
            "all": "All",
            "subject": "Subject",
            "subtitle": "Subtitle text",
            "template": "Choose a template",
            "text": "Body text",
            "send": "Send"
        },
        "home": {
            "online": "Online users",
            "registered": "Registered users",
            "enrolled": "Enrolled users",
            "our": "Our",
            "git": "GitLab API Status",
            "gitOk": "Operational",
            "gitNok": "Not working"
        },
        "links": {
            "title": "Links",
            "text": "URL Text",
            "url": "URL",
            "language": "Language",
            "languages": "Languages",
            "name": "Name"
        },
        "templates": {
            "title": "Define a new email template",
            "name": "Name",
            "desc": "Description",
            "template": "Template",
            "cname": "course or specialization name",
            "amount": "paid amount"
        },
        "course": {
            "visibility": "Visibility",
            "hidden": "Hidden",
            "only": "Only for mentors",
            "wip": "WIP",
            "level": "Level",
            "mentors": "Mentors",
            "new_mentor": "Add a mentor to this course",
            "categories": "Categories",
            "new_category": "Add this course to the category",
            "title": "Topics",
            "name": "Name",
            "desc": "Description",
            "languages": "Languages",
            "status": "GitLab Hook Status"
        },
        "press": {
            "title": "Press",
            "name": "Name",
            "date": "Date",
            "languages": "Languages",
            "url": "URL",
            "type": "Type",
            "language": "Language"
        },
        "seed": {
            "title": "Seed importer",
            "seed": "Seed",
            "language": "Language"
        },
        "specialization": {
            "modify": "Modify",
            "name": "Name",
            "desc": "Description",
            "image": "Picture",
            "drop": "Drop here or",
            "visibility": "Visibility",
            "hidden": "Hidden",
            "only": "Only for mentors",
            "language": "Language",
            "salary": "Average salary for specialization",
            "salaries": "Salaries",
            "colum": "Column index",
            "define": "Define a new specialization",
            "title": "Specializations"
        },
        "team": {
            "title": "Team Members",
            "name": "Name",
            "pos": "Position",
            "bio": "Biography",
            "image": "Image",
            "change": "Change image"
        },
        "testimonials": {
            "title": "Testomonials",
            "picture": "Picture",
            "from": "From",
            "date": "Date",
            "language": "Language",
            "url": "URL",
            "text": "Text"
        },
        "users": {
            "user": "User",
            "basic": "Basic info",
            "roles": "Roles",
            "picture": "Picture",
            "city": "City",
            "state": "Country",
            "uuid": "UUID",
            "index": "Index",
            "currency": "Currency",
            "language": "Language",
            "online": "Online",
            "push": "Push enabled",
            "change": "Change info",
            "yes": "Yes",
            "no": "No",
            "title": "Users",
            "everyone": "Everyone",
            "students": "Students",
            "mentors": "Mentors",
            "name": "Name",
            "prev": "Previous",
            "next": "Next"
        },
        "save": "Save changes!",
        "add": "Add new!",
        "markdown": "Markdown is supported!",
        "edit": "Edit",
        "delete": "Delete"
    },
    "course": {
        "title": "Crowdsourced topic",
        "learn": "Learn for free",
        "learnProgress": "Learning in progress",
        "login": "Login to start",
        "interested": "I'm interested",
        "discussion": "View discussion",
        "menu": {
            "about": "About",
            "corpus": "Corpus",
            "updates": "Log"
        },
        "manifest": {
            "contribs": "contributors",
            "langs": "languages",
            "suggestions": "suggestions",
            "contribute": "Contribute",
            "title": "About this course",
            "mentors": "Mentors",
            "lessons": "Lessons",
            "projects": "Projects",
            "tasks": "Tasks",
            "cooperation": "In cooperation with"
        },
        "lesson": {
            "improve": "Improve this",
            "prev": "Previous",
            "next": "Next",
            "back": "Back to topic"
        }
    },
    "search": {
        "categories": "Categories",
        "level": "Level",
        "beginner": "Beginner",
        "intermediate": "Intermediate",
        "advanced": "Advanced",
        "contribs": "Contributions",
        "suggestions": "Suggestions",
        "suggest": "Suggest",
        "notFound": "Not seeing your topic here? You can suggest",
        "notFound2": "right now"
    },
    "specialization": {
        "title": "Path",
        "learn": "Learn for free",
        "menu": {
            "about": "About",
            "programme": "Programme",
            "meta": "Metadata"
        },
        "info": {
            "title": "About this specialization",
            "contribs": "Contributors",
            "mentors": "Mentors",
            "salary": "Average salary"
        }
    },
    "profile": {
        "edit": {
            "title": "Edit you profile",
            "menu": {
                "personal": "Personal",
                "integrations": "Integrations",
                "payment": "Payment",
                "notifications": "Notifications"
            },
            "personal": {
                "title": "Personal",
                "subtitle": "Basic information about you",
                "name": "Full name",
                "email": "E-mail",
                "social": "Social",
                "profiles": "Your social profiles. If you're looking for integrations click the",
                "integration": "Integrations",
                "tab": "tab",
                "image": "Image",
                "change": "Change image",
                "security": "Account security",
                "secinfo": "Manage your accounts' more sensitive side",
                "password": "Manage passwords",
                "cache": "Local cache",
                "cacheinfo": "If you experience any problems with our platform, try clearing your local cache.",
                "reset": "Clear cache",
                "delete": "Delete account",
                "deleteInfo": "If you no longer wish to use our services, you can delete your account here. All your private data will be removed."
            },
            "integrations": {
                "gitlab": {
                    "title": "Connect with GitLab",
                    "subtitle": "Integrate your Gitlab to contribute to Zamphyr",
                    "integrate": "Integrate with Gitlab"
                },
                "facebook": {
                    "title": "Connect with Facebook",
                    "subtitle": "Integrate your Facebook and let people know about your achievements and get tailored recommendatons",
                    "integrate": "Connect with Facebook"
                },
                "slack": {
                    "title": "Connect with Slack",
                    "subtitle": "Integrate your Slack account to get changes and notifications in real-time",
                    "integrate": "Integrate with Slack"
                },
                "connected": "Service already connected."
            },
            "interactions": {
                "title": "Interactions",
                "subtitle": "Choose how Asha interacts with you",
                "basic": "Ask me questions to improve my experience and success",
                "others": "Connect me to other students with similar preferences",
                "tailor": "Tailor offers to fit my goals",
                "miner": "Use a small amount of my resources to mine Monero coins."
            },
            "email": {
                "title": "E-mail communication",
                "subtitle": "Choose when to get notified about things via e-mail",
                "basic": "Get information related to your account and security",
                "announcements": "Get announcements and information about platform changes",
                "promo": "Get messages related to promotions and community involvement",
                "newsletter": "Get our newsletter"
            }
        },
        "roles": {
            "korisnik": "User",
            "upravnik": "Administrator",
            "mentor": "Mentor",
            "predavac": "Teacher",
            "premium": "Premium",
            "student": "Student"
        },
        "menu": {
            "info": "Information",
            "courses": "Courses",
            "accomplishments": "Accomplishments",
            "entities": "Entities",
            "invoices": "Invoices"
        },
        "courses": {
            "enrolled": "Enrolled",
            "linkedin": "Share on LinkedIn",
            "facebook": "Share on Facebook"
        },
        "entities": {
            "title": "Your entities"
        },
        "invoices": {
            "title": "Your invoices",
            "cancel": "Cancel"
        }
    },
    "simple": {
        "register": {
            "title": "Welcome to the family",
            "subtitle": "To experience the full potential of our next generation school we tailor the school to fit you. Make it better now by telling us a bit about yourself.",
            "share": "Share to finish",
            "noshare": "I don't want to share with others",
            "menu": {
                "discover": "Explore",
                "join": "Join",
                "improve": "Make it better",
                "success": "Success"
            },
            "work": {
                "title": "Work experience",
                "no": "Not looking for a job",
                "working": "Happily employed",
                "looking": "Looking for a change",
                "active": "Actively searching",
                "internship": "Looking for an internship"
            },
            "level": {
                "title": "Knowledge level",
                "beginner": "Starting from scratch",
                "basic": "Basic",
                "advanced": "Advanced"
            },
            "terms1": "By continuing you agree to",
            "terms2": "usage terms",
            "terms3": "of our service",
            "name": "Full name",
            "asha": "Allow data processing by our AI, Asha",
            "email": "E-mail",
            "city": "City",
            "state": "Country",
            "language": "Language"
        },
        "notifications": {
            "filters": "Filters",
            "unread": "Unread",
            "all": "All notifications",
            "apps": "Applications",
            "asha": "Asha",
            "title": "Notifications"
        },
        "notAllowed": {
            "title": "Not allowed!"
        },
        "media": {
            "title": "Media library",
            "uploading": "Uploading...",
            "drag": "Drag or click button to start upload",
            "upload": "Upload media",
            "images": "Images",
            "preview": "Preview",
            "about": "About media",
            "message": "The file is not selected."
        }
    },
    "nav": {
        "search": "Search for paths or topics",
        "login": "Login",
        "contribute": "Contribute",
        "notifications": "See all notifications",
        "collective": "Collective",
        "meta": "Meta",
        "community": "Community",
        "account": "Account",
        "admin": "Administration",
        "settings": "Settings",
        "logout": "Log out",
        "learning": "https://www.facebook.com/groups/learnwithzamphyr/"
    },
    "footer": {
        "quest": "What is Zamphyr?",
        "ans": "Zamphyr is a platform challenging the status quo in education using crowdsourcing. We're teaching teachnology to the next generation and building the next iteration of a school.\nEnthusiasts, experts, tech companies and universities can contribute.",
        "more": "More about contributing",
        "copy": "Sva prava zadržana. Sva registovana imena, oznake i žigovi pripadaju njihovim vlasnicima. Korišćenje imena, oznaka ili žiga ne označava podršku niti afilijaciju sa vlasnicima istih osim u slučajevima kada je podrška ili afilijacija eksplicitno označena.",
        "head1": "You + Zamphyr",
        "head1_story": "Our story",
        "head1_jobs": "Careers",
        "head1_faq": "FAQ",
        "head1_contrib": "Contribute",
        "head1_mentor": "Become a mentor",
        "head2": "For business",
        "head2_data": "Data platform",
        "head2_hire": "Hire graduates",
        "head2_partner": "Partnerships",
        "head3": "For developers",
        "head3_internship": "Apply for an internship",
        "head3_api": "API",
        "head4": "Follow us",
        "head4_follow": "Follow our bulletin",
        "head4_blog": "Blog",
        "head5": "Support",
        "head6": "Change language",
        "privacy": "Privacy policy",
        "tos": "Terms of service"
    },
    "tos": {
        "title": "Terms of Service",
        "def": "Definitions",
        "paragraphs": {
            "p1": "These Terms of Service, together with our Privacy Policy (collectively “Terms”), govern your use of the services (“Services”) and content (“Service Content”) that we, Zamphyr OÜ, an Estonian private limited company, registry number 14197472, headquartered at Sepapaja 6, Ülemiste City, 15551 Tallinn, Harju maakond, Estonia, e-mail: vision@zamphyr.com, (or “Zamphyr”) provide to you via our infrastructure. By using our Services, you accept these Terms in full. If you disagree with these Terms or any part thereof, you must not use the Services.",
            "p2": "We do our best but we cannot guarantee your success in learning. We also assume no responsibility for the results of your future use of the skills that you may or may not have learned with us. While we are in testing, alpha or beta parts of our services may be not yet functional, broken, or changed without warning. This also means that we may even terminate some or all of our services, or your access to them, without warning.",
            "p3": "You hereby indemnify us and undertake to keep us indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by us to a third party in settlement of a claim or dispute on the advice of our legal advisers) incurred or suffered by us arising out of any breach by you of any provision of these Terms.",
            "p4": "These Terms will be governed by and construed in accordance with the laws of Republic of Estonia, and any disputes relating to these Terms will be subject to the exclusive jurisdiction of the courts of Estonia.",
            "p5": "You grant to us a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media. You also grant to us the right to sub-license these rights, and the right to bring an action for infringement of these rights."
        },
        "title": {
            "t2": "Introduction",
            "t3": "Indemnity",
            "t4": "Jurisdiction",
            "t5": "CLA"
        },
        "list": {
            "l1": "Your data, accounts and personal information",
            "l2": "UGC",
            "l3": "Disclaimers"
        }
    },
    "notifications": {
        "facebook": {
            "title": "<b>Facebook</b> has a recommendation for you",
            "text": "You can get help from other students by joining our study group on Facebook.",
            "button": "Join the study group"
        },
        "integrations": {
            "title": "Update your integrations",
            "text": "If you haven't done so already, you can integrate accounts from various other platforms with our platform.",
            "button": "Update right now"
        },
        "newsletter": {
            "title": "Subscribe to our newsletter",
            "text": "We believe that subscribing to our newsletter is rather beneficial.",
            "button_1": "Sure, I want to subscribe.",
            "button_2": "Not now."
        },
        "linkedin": {
            "title": "Update your LinkedIn profile",
            "text": "You have completed a new course, share the word, add your accomplishment to your LinkedIn profile.",
            "button": "Right away."
        },
        "update": {
            "title": "Something has changed.",
            "text": "New content arrived on one of your courses, check it out.",
            "button": "Heck yea."
        },
        "medium": {
            "title": "Subscribe to our blog",
            "text": "Don't miss another post, subscribe to our blog now.",
            "button": "I'm in."
        }
    },
    "asha": {
        "question": "<b>Asha</b> has a question for you",
        "push": "Asha has sent you a question on Zamphyr! Check it out.",
        "email": "Asha has sent you a question on Zamphyr"
    },
    "errors": {
        "title": "Error!",
        "predlog_postoji": "The proposal with the identical name already exists.",
        "sva_polja": "You need to fill all required fields!",
        "fragment_postoji": "Fragment with identical name already exists!",
        "login": "You must be logged in!",
        "jezik": "Language does not exist.",
        "predmet_nivo": "You do not teach the subject for which you want to add a level!",
        "server": "The metod can be called only from the server!",
        "predmet_novost": "you do not teach the subject for which you want to add a news ",
        "novost_autor": "You are not the author of the given news!",
        "nema_predmeta": "Subject does not exist!",
        "slack": "Error during the call on Slack!",
        "predmet_modul": "You do not teach the subject for which do you want to add module!",
        "predmet_resurs": "You do not teach the subject for which you want to add a resource!",
        "predmet_zadatak": "You do not teach the subject for which you want to add a task!",
        "predmet_test": "You do not teach the subject for which you want to add a test!",
        "gitlab_api": "Wrong GitLab API key!",
        "predmet_preduslov": "The subject can not be a prerequisite by itself!",
        "predmet_postuslov": "The subject can not be a postcondition by itself!",
        "facebook": "Error during sending on Facebook page.",
        "dob_lok": "Date of birth and location must not be empty!",
        "kupon_iskoriscen": "The coupon has already been used. If you think that this is an error send us a message on support@zamphyr.com",
        "kupon_ne_vazi": "The coupon is not valid for this object. If you think that this is an error send us a message on support@zamphyr.com",
        "ne_predavac": "You are not the lecturer!",
        "predmet_token": "Token for adding a subject is wrong!",
        "predavac": "The lecturer already teaches the subject!",
        "predmet_kategorija": "The subject is already in that category!",
        "gitlab_username": "GitLab username does not exist!",
        "predlog_spoji": "You can not match the same proposals.",
        "predlog_spoji2": "You can not match something with empty proposal.",
        "nema_firme": "Entity does not exist.",
        "postgres": "PostgreSQL error.",
        "postgres_no": "PostgreSQL server is not online.",
        "postgres_close": "Error while closing connection with PostgreSQL server.",
        "no_file": "No file.",
        "new_course": "Error while creating a new course.",
        "sending_email": "Error while sending the email.",
        "generic": "An error occurred.",
        "no_access": "You can view all topics for free when you're logged in.",
        "no_access_create1": "Create an account",
        "no_access_create2": "login",
        "no_access_create4": "or",
        "no_access_create3": "to access the entire corpus.",
        "lekcija_nedostupna": "Current lesson is not yet available. Please check again later.",
        "description": "No description available."
    },
    "info": {
        "action": "Thank you for your action.",
        "uploading": "Uploading...",
        "predavac_dodat": "The lecturer added successfully.",
        "kategorija_dodata": "The category is added succesfully.",
        "import": "Importing...",
        "prosao": "Passed!",
        "nije_prosao": "Fail!",
        "izmene": "Changes are successfully saved!",
        "upisan": "User is successfully added!",
        "confirm_course": "Are you sure you want to remove this course?",
        "new_notification": "New notification",
        "integrateGitlab": "Integrate your GitLab account",
        "integrateFacebook": "Integrate your Facebook account",
        "cache": "Cache has been succesfully cleared!"
    }
}